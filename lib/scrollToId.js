export default function scrollNavigate(hash) {
	let id = document.querySelector(hash)
	if (id) {
		scrollToId(id)
	}
}
function scrollToId(id) {
	let sectionOffsetTop = id.offsetTop
	let offsetTop = 0
	if (window.innerWidth <= 992){
		offsetTop = 70
	}

	window.scrollTo({
		top: sectionOffsetTop - offsetTop,
		behavior: "smooth"
	})
}
"use client"


import Modal from 'react-modal';
import { useSelector, useDispatch } from 'react-redux';
import { showModalForm, hideModalForm } from '@/store/formModal';
import Form from "@/components/form";

const customStyles = {
	content: {
		top: '50%',
		left: '50%',
		right: 'auto',
		bottom: 'auto',
		marginRight: '-50%',
		transform: 'translate(-50%, -50%)',
		border: 'none',
		background: 'none'
	},
};

Modal.setAppElement('#root');
export default function ModalForm() {
	const modalIsOpen = useSelector((state) => state.formModal);
	const dispatch = useDispatch();
	function openModal() {
		dispatch(showModalForm())
	}

	function closeModal() {
		dispatch(hideModalForm())
	}

	return (
			<div>
				<Modal
						isOpen={modalIsOpen}
						onRequestClose={closeModal}
						style={customStyles}
						contentLabel="Example Modal"
				>
					<div className="modal-wrapper">
						<div className="flex modal-action">
							<img src="/image/modal-stars.png" alt="stars" width="303" height="376"
							     className="img modal-stars"/>
							<button className="btn close" title="close dialog" role="button" onClick={closeModal}>
								<img src="/image/close.svg" alt="close" width="116" height="116" className="img"/>
							</button>
						</div>
						<div className="modal-text">
							<h2 className="title">
								Join our exclusive alpha list for early access and exciting launch updates!
							</h2>
						</div>
						<div className="modal-form">
							<Form />
						</div>
					</div>
				</Modal>
			</div>
	);
}
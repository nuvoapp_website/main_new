'use client';

import Image from "next/image";

export default function SocialLinks() {
	return (
			<ul className="flex">
				<li>
					<a href="" target="_blank" title="twitter">
						<Image src="/image/twitter.svg" alt="twitter" className="img" width={39} height={39}/>
					</a>
				</li>
				<li>
					<a href="" target="_blank" title="youtube">
						<Image src="/image/youtube.svg" alt="youtube" width={39} height={39} className="img"/>
					</a>
				</li>
				<li>
					<a href="" target="_blank" title="facebook">
						<Image src="/image/facebook.svg" alt="facebook" width={39} height={39} className="img"/>
					</a>
				</li>
				<li>
					<a href="" target="_blank" title="instagram">
						<Image src="/image/insta.svg" alt="instagram" width={35} height={35} className="img"/>
					</a>
				</li>
				<li>
					<a href="" target="_blank" title="linkedin">
						<Image src="/image/linkedin.svg" alt="linkedin" width={39} height={39} className="img"/>
					</a>
				</li>
			</ul>
	)
}
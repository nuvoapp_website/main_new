"use client"

import {useState} from "react";
import Modal from 'react-modal';

import { useSelector, useDispatch } from 'react-redux';
import { showModal, hideModal} from '@/store/modalSuccess';

const customStyles = {
	content: {
		top: '50%',
		left: '50%',
		right: 'auto',
		bottom: 'auto',
		marginRight: '-50%',
		transform: 'translate(-50%, -50%)',
		border: 'none',
		background: 'none'
	},
};

Modal.setAppElement('#root');
export default function ModalSuccess() {
	const modalIsOpen = useSelector((state) => state.modalSuccess);
	const dispatch = useDispatch();

	function openModal() {
		dispatch(openModal());
	}

	function closeModal() {
		dispatch(hideModal());
	}

	return (
			<div>
				<Modal
						isOpen={modalIsOpen}
						onRequestClose={closeModal}
						style={customStyles}
						contentLabel="Example Modal"
				>
					<div className="modal-wrapper">
						<div className="flex modal-action">
							<div className="modal-logo-wrapper">
								<img src="/image/modal-logo.png" alt="stars" width="699" height="348" className="img modal-logo"/>
							</div>
							<button className="btn close" onClick={closeModal} title="close dialog" role="button">
								<img src="/image/close.svg" alt="close" width="116" height="116" className="img"/>
							</button>
						</div>
						<div className="modal-text modal-text-success">
							<h2 className="title">Thanks for registering!</h2>
							<p className="text-small">We'll notify you when we go live. Stay tuned <br/> for exciting updates!</p>
						</div>
						<div className="modal-success-action flex">
							<button className="btn btn-primary" title="close" onClick={closeModal}>
								Close
							</button>
						</div>
					</div>
				</Modal>
			</div>
	);
}
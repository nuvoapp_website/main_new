import { useState } from "react";
import axios from "axios";
import { useSelector, useDispatch } from 'react-redux';
import { showModalForm, hideModalForm } from '@/store/formModal';
import { showModal, hideModal } from '@/store/modalSuccess';

export default function Form() {
	const [email, setEmail] = useState('')
	const [isLoading, setIsLoading] = useState(false)
	const [errorMessage, setErrorMessage] = useState(null)
	const dispatch = useDispatch();
	let error = true

	function validateEmail() {
		let characters = /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z]+$/;
		if (!characters.test(email) || email.length <= 0) {
			error = true
			setErrorMessage('Invalid Email Address')
		} else {
			error = false
			setErrorMessage(null)
		}
	}

	async function onSubmit(event) {
		event.preventDefault()

		validateEmail()
		let formData = new FormData()
		formData.append('Email:', email)

		if (!error){
			try {
				await setIsLoading(true)
				const response = await axios({
					method: 'POST',
					url: 'https://backend.nuvoapp.co/form.php',
					data: formData,
					headers: {"Content-Type": "multipart/form-data"},
				})
				dispatch(hideModalForm())
				dispatch(showModal())
			} catch (err) {
				setErrorMessage(err.message)
				console.error(err)
			} finally {
				setIsLoading(false)
			}
		}
	}

	return (
			<div>
				<form className="flex form" noValidate onSubmit={ onSubmit }>
					<label htmlFor="forEmail" className={errorMessage ? 'danger' : ''}>

						{error && <span className='error'>{errorMessage}</span>}

						<input
								type="email"
								name="email"
								value={email}
								onChange={e => setEmail(e.target.value)}
								onBlur={validateEmail}
								placeholder="Enter your email address"
								className="form-control"
								id="forEmail"
						/>

					</label>
					<button type="submit" className="btn btn-primary" disabled={isLoading}>
						Let the journey begin!
					</button>
				</form>
			</div>
	)
}
'use client';

import { useTranslation } from 'react-i18next';
import Image from 'next/image';
import Navigation from "@/components/Navigation";
import SocialLinks from "@/components/SocialLinks";
import Link from "next/link";
import scrollNavigate from "@/lib/scrollToId";


export default function Footer() {
	const {t} = useTranslation();

	const onScroll = (event, hash) => {
		event.preventDefault()
		scrollNavigate(hash)
	}

	return(
			<footer className="footer">
				<div className="container">
					<div className="flex footer-wrapper">
						<div className="flex footer-item footer-logo">
							<Link href="/" className="logo">
								<Image src="/image/logo-green.svg" alt="NUVO" width={238} height={75} className="img" />
							</Link>
							<div className="flex footer-policy">
								<a href="">Privacy Policy</a>
								<Image src="/image/dot.svg" alt="icon" width={3} height={4} className="img" />
									<a href="">Terms & conditions</a>
							</div>
						</div>
						<div className="flex footer-item footer-item-nav">
							<nav className="footer-nav">
								<ul className="flex navigate-block">
									<Navigation hideMenu={() => false}/>
								</ul>
							</nav>
						</div>
						<div className="flex footer-item footer-item-action navigate-block">
							<Link href="#topSection" className="btn navigate" title="Back to Top" onClick={(event) => onScroll(event, '#topSection')}>
								<Image src="/image/toTop.svg" alt="top" width={19} height={18} className="img" />
								<span>Back to Top</span>
							</Link>
						</div>
						<div className="flex footer-item footer-item-social">
							<nav className="footer-social">
								<SocialLinks />
							</nav>
						</div>
						<div className="flex footer-item footer-email">
							<a href="" className="btn btn-outline" title="Email">
								hello@nuvoapp.co
							</a>
							<p>© NUVO 2023</p>
						</div>
					</div>
				</div>
			</footer>
	)
}
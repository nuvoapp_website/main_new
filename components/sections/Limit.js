'use client';

import { useTranslation } from 'react-i18next';
import Image from 'next/image';
export default function Limit() {
	const {t} = useTranslation();
	return(
			<section className="section-limit" id="limit">
				<div className="container">
					<div className="flex limit-wrapper">
						<div className="limit-img">
							<div className="limit-img-content">
								<div className="limit-stars">
									<Image src="/image/star.svg" width={74} height={74} alt="star" className="img limit-star-1" />
									<Image src="/image/star.svg" width={74} height={74} alt="star" className="img limit-star-2" />
									<Image src="/image/star-outline.svg" width={59} height={57} alt="star" className="img limit-star-3" />
								</div>
								<Image src="/image/ellipse.svg" width={743} height={744} alt="limit" className="img ellipse" />
									<p className="limit-string">
										<span>{ t('limit:limitTitle1') }</span>
										<span>{ t('limit:limitTitle2') }</span>
										<span>{ t('limit:limitTitle3') }</span>
									</p>
							</div>
						</div>
						<div className="limit-text">
							<Image src="/image/icon1.svg" width={199} height={199} alt="icon" className="img" />
								<p className="text-small">
									{ t('limit:text') }
								</p>
								<a href="" className="btn btn-primary" title={ t('limit:button') }>{ t('limit:button') }</a>
						</div>
					</div>
				</div>
			</section>
	)
}
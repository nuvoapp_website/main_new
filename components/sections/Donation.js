'use client';

import { useTranslation } from 'react-i18next';
import Image from 'next/image';


export default function Donation() {
	const {t} = useTranslation();
	return(
			<section className="section-donation" id="donation">
				<div className="container">
					<div className="flex donation-wrapper">
						<div className="donation-text">
							<h2 className="title">{ t('donation:title') }</h2>
							<p className="text-lg">
								{ t('donation:text') }
							</p>
							<div className="flex donation-actions">
								<div className="flex donation-social">
									<Image src="/image/logo-outline.svg" alt="logo" width={205} height={75} className="img logo-outline" />
										<a href="" title="instagram">
											<svg width="75" height="75" viewBox="0 0 75 75" fill="none" xmlns="http://www.w3.org/2000/svg">
												<rect x="2.46578" y="1.96578" width="70.5684" height="70.5684" rx="35.2842" stroke="black" strokeWidth="3.93157"/>
												<path d="M47.8521 25.8345H50.7401M29.0801 17.1705H46.4081C52.7881 17.1705 57.9601 22.3425 57.9601 28.7225V46.0505C57.9601 52.4305 52.7881 57.6026 46.4081 57.6026H29.0801C22.7001 57.6026 17.5281 52.4305 17.5281 46.0505V28.7225C17.5281 22.3425 22.7001 17.1705 29.0801 17.1705ZM37.7441 46.0505C32.9591 46.0505 29.0801 42.1715 29.0801 37.3865C29.0801 32.6015 32.9591 28.7225 37.7441 28.7225C42.5291 28.7225 46.4081 32.6015 46.4081 37.3865C46.4081 42.1715 42.5291 46.0505 37.7441 46.0505Z" stroke="#070000" strokeWidth="4.332"/>
											</svg>
										</a>
										<a href="" title="TikTok">
											<svg width="75" height="75" viewBox="0 0 75 75" fill="none" xmlns="http://www.w3.org/2000/svg">
												<rect x="2" y="2" width="70.5" height="70.5" rx="35.25" stroke="black" strokeWidth="4"/>
												<path d="M41.7767 20.75V45.0966C41.7767 49.3749 38.3084 52.8432 34.0301 52.8432C29.7517 52.8432 26.2834 49.3749 26.2834 45.0966C26.2834 40.8182 29.7517 37.3499 34.0301 37.3499M51.7367 32.9233C46.236 32.9233 41.7767 28.4641 41.7767 22.9633" stroke="black" strokeWidth="4.14999"/>
											</svg>
										</a>
								</div>
								<a href="" className="btn btn-primary" title={ t('donation:button') }>{ t('donation:button') }</a>
							</div>
						</div>
						<div className="donation-img">
							<Image src="/image/icon2.svg" alt="icon" width={77} height={78} className="img icon" />
							<Image src="/image/donation-person.webp" alt="donation" width={348} height={450} className="img person" />
						</div>
					</div>
				</div>
			</section>
	)
}	
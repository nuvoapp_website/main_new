'use client';

import { useTranslation } from 'react-i18next';
import Image from 'next/image';


export default function QrSection() {
	const {t} = useTranslation();

	return(
			<section className="section-qr" id="qr">
				<div className="container">
					<div className="flex qr-wrapper">
						<div className="qr-img">
							<div className="qr-stars">
								<Image src="/image/star.svg" width={74} height={74} alt="star" className="img qr-star-1"/>
								<Image src="/image/star.svg" width={74} height={74} alt="star" className="img qr-star-2"/>
								<Image src="/image/star.svg" width={74} height={74} alt="star" className="img qr-star-3"/>
								<Image src="/image/star.svg" width={74} height={74} alt="star" className="img qr-star-4"/>
								<Image src="/image/star-outline.svg" width={59} height={57} alt="star" className="img qr-star-5"/>
							</div>
							<div className="qr-person">
								<Image src='/image/qr-person.png' width={500} height={600} alt='qr'/>
							</div>
						</div>
						<div className="qr-text">
							<Image src="/image/icon3.svg" alt="icon" width={123} height={123} className="img" />
								<h2 className="title">{ t('qr:title') }</h2>
								<p className="text-small">
									{ t('qr:text') }
								</p>
								<a href="" className="btn btn-primary" title="Tell Me More">{ t('qr:button') }</a>
						</div>
					</div>
				</div>
			</section>
	)
}
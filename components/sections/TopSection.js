'use client';

import Image from 'next/image';
import Link from "next/link";
import { useTranslation } from 'react-i18next';
import Header from "@/components/Header";
import scrollNavigate from "@/lib/scrollToId";

import { useSelector, useDispatch } from 'react-redux';
import { showModalForm, hideModalForm } from '@/store/formModal';

export default function TopSection() {
	const { t } = useTranslation();
	const modal = useSelector((state) => state.formModal);
	const dispatch = useDispatch();

	const onScroll = (event, hash) => {
		event.preventDefault()
		scrollNavigate(hash)
	}
	function modalOpen(){
		dispatch(showModalForm())
	}

	return (
			<section className="flex top-section" id="topSection">
				<div className="top-card">
					<Header />
					<div className="container">
						<div className="flex top-section-content">
							<div className="top-section-text">
								<h1>{ t('top-section:header_title') }</h1>
								<p>{ t('top-section:header_text') }</p>
								<button onClick={modalOpen} className="btn btn-primary" title="Get started">{ t('top-section:header_button') }</button>
							</div>
							<div className="top-section-img">
								<div className="stars-wrapper">
									<Image src="/image/star.svg" width={74} height={74} alt="star" className="img star-1"/>
									<Image src="image/star.svg" width={74} height={74} alt="star" className="img star-2"/>
									<Image src="/image/star.svg" width={74} height={74} alt="star" className="img star-3"/>
									<Image src="/image/star.svg" width={74} height={74} alt="star" className="img star-4"/>
									<Image src="/image/star.svg" width={74} height={74} alt="star" className="img star-5"/>
									<Image src="/image/star-outline.svg" width={59} height={57} alt="star" className="img star-6"/>
									<Image src="/image/star-outline.svg" width={59} height={57} alt="star" className="img star-7"/>
								</div>
								<div className="card-img">
									<img
											src="/image/card-700.png"
											alt="Card"
											width={700}
											height={755}
											className="img card"
									/>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div className="flex top-section-action navigate-block">
					<Link href="#limit" className="btn navigate" onClick={(event) => onScroll(event, '#limit')} title="scroll down">
						<span>{ t('top-section:scroll') }</span>
						<img src="/image/scroll.svg" alt="scroll" width={12} height={27}/>
						<span>{ t('top-section:down') }</span>
					</Link>
				</div>
			</section>
	);
}
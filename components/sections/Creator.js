'use client';

import { useTranslation } from 'react-i18next';
import Image from 'next/image';


export default function Creator() {
	const {t} = useTranslation();
	return(
			<section className="creator-section" id="creator">
				<div className="container">
					<div className="creator-wrapper">
						<Image src="/image/creator.webp" alt="creator" width={1467} height={389} className="img" />
							<p className="text-small">
								{ t('creator:text') }
							</p>
							<a href="" className="btn btn-primary" title="Explore Community">{ t('creator:button') }</a>
					</div>
				</div>
			</section>
	)
}	
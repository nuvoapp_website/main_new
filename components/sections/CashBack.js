'use client';

import { useTranslation } from 'react-i18next';
import Image from 'next/image';


export default function CashBack() {
	const {t} = useTranslation();
	return(
			<section className="cashback-section" id="cashback">
				<div className="container">
					<div className="flex cashback-wrapper">
						<div className="cashback-text">
							<Image src="/image/icon4.svg" alt="icon" width={105} height={104} className="img" />
								<h2 className="title">{ t('cashback:title') }</h2>
								<p className="text-lg">
									{ t('cashback:text') }
								</p>
								<a href="" className="btn btn-primary" title={ t('cashback:button') }>{ t('cashback:button') }</a>
						</div>
						<div className="cashback-img">
							<Image src="/image/cashback.png" alt="cashback" width={804} height={994} className="img" />
						</div>
					</div>
				</div>
			</section>
	)
}
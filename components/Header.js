'use client';

import {useState} from "react";
import Image from 'next/image';
import Link from "next/link";
import { useTranslation } from 'react-i18next';
import LanguageChanger from "@/components/LanguageChanger";
import Navigation from "@/components/Navigation";
import SocialLinks from "@/components/SocialLinks";
import MobileHeader from "@/components/MobileHeader";

import { useSelector, useDispatch } from 'react-redux';
import { showModalForm, hideModalForm } from '@/store/formModal';

export default function Header() {
	const [showMenu, setShowMenu] = useState(false)
	const {t} = useTranslation();
	const dispatch = useDispatch();

	let classMenu = ['top-header']

	if (showMenu){
		classMenu.push('active')
	}

	function modalOpen(){
		dispatch(showModalForm())
	}


	return(
			<>
				<MobileHeader showMenu={showMenu} setShowMenu={setShowMenu}/>
				<header className={classMenu.join(' ')}>
					<div className="container">
						<div className="flex top-header-wrapper">
							<Link href="/" className="logo">
								<Image src="/image/logo.svg" width={122} height={41} alt="Nuvo" className="img"/>
							</Link>
							<LanguageChanger/>
							<div className="grow"></div>
							<nav className="header-nav">
								<ul className="flex header-nav-list navigate-block">
									<Navigation hideMenu={setShowMenu}/>
								</ul>
							</nav>
							<nav className="header-social">
								<SocialLinks/>
							</nav>
							<button onClick={modalOpen} className="btn btn-outline btn-small btn-modal">
								{t('top-section:header_button')}
							</button>
						</div>
					</div>
				</header>
			</>
	)
}
'use client';

import { useTranslation } from 'react-i18next';
import Link from "next/link";
import scrollNavigate from '@/lib/scrollToId'
export default function Navigation({hideMenu}) {
	const { t } = useTranslation();
	const onScroll = (event) => {
		event.preventDefault()
		hideMenu(false)
		scrollNavigate(event.target.hash)
	}


	return (
			<>
				<li><Link href="#limit" className="navigate" onClick={onScroll}>{ t('menu:Limits') }</Link></li>
				<li><Link href="#donation" className="navigate" onClick={onScroll}>{ t('menu:Donation') }</Link></li>
				<li><Link href="#qr" className="navigate" onClick={onScroll}>{ t('menu:QR-Tip') }</Link></li>
				<li><Link href="#creator" className="navigate" onClick={onScroll}>{ t('menu:Community') }</Link></li>
				<li><Link href="#cashback" className="navigate" onClick={onScroll}>{ t('menu:Cashback') }</Link></li>
				<li><Link href="">Contact</Link></li>
			</>
	);
}
import {useEffect} from "react";
import Link from "next/link";
import Image from "next/image";

export default function MobileHeader({showMenu, setShowMenu}) {
	let classMenuBtn = ['flex', 'btn', 'btn-menu']

	if (showMenu){
		classMenuBtn.push('open')
		if (typeof window !== "undefined") {
			document.querySelector('body').classList.add('open')
		}
	} else {
		if (typeof window !== "undefined") {
			document.querySelector('body').classList.remove('open')
		}
	}
	const menuHandler = () => {
		setShowMenu(!showMenu)
	}
	useEffect(() => {
		window.addEventListener('scroll', activeMenuColor)
		function activeMenuColor() {
			if(window.scrollY >= document.querySelector('.top-section').clientHeight) {
				document.querySelector('.mobile-header').classList.add('active')
			} else {
				document.querySelector('.mobile-header').classList.remove('active')
			}
		}
	}, []);

	return (
			<header className="mobile-header">
				<div className="container">
					<div className="flex mobile-header-wrapper">
						<Link href="/" className="logo">
							<Image src="/image/logo.svg" width={122} height={41} alt="Nuvo" className="img"/>
						</Link>
						<button
								role="button"
								className={classMenuBtn.join(' ')}
								aria-label="Menu Button"
								onClick={menuHandler}>
						</button>
					</div>
				</div>
			</header>
	)
}
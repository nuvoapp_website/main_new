import TopSection from "@/components/sections/TopSection";
import Limit from "@/components/sections/Limit";
import Donation from "@/components/sections/Donation";
import QrSection from "@/components/sections/QrSection";
import Creator from "@/components/sections/Creator";
import CashBack from "@/components/sections/CashBack";
import Footer from "@/components/sections/Footer";
import ModalForm from "@/components/ModalForm";
import ModalSuccess from "@/components/ModalSuccess";
export default async function Home() {
  return (
      <div id="root">
        <TopSection/>
        <Limit/>
        <Donation/>
        <QrSection/>
        <Creator/>
        <CashBack/>
        <Footer/>
        <ModalForm/>
        <ModalSuccess/>
      </div>
  );
}
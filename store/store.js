import { configureStore } from '@reduxjs/toolkit';
// Import your reducers
import formModal from './formModal';
import modalSuccess from './modalSuccess';

const store = configureStore({
	reducer: {
		formModal,
		modalSuccess
	},
});
export default store;
import { createSlice } from '@reduxjs/toolkit';
const modalFormSlice = createSlice({
	name: 'formModal',
	initialState: false,
	reducers: {
		showModalForm: (state) => state = true,
		hideModalForm: (state) => state  = false,
	},
});
export const { showModalForm, hideModalForm } = modalFormSlice.actions;
export default modalFormSlice.reducer;
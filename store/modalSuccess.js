import { createSlice } from '@reduxjs/toolkit';
const modalSuccess = createSlice({
    name: 'modalSuccess',
    initialState: false,
    reducers: {
        showModal: (state) => state = true,
        hideModal: (state) => state  = false,
    },
});
export const { showModal, hideModal } = modalSuccess.actions;
export default modalSuccess.reducer;